ARG JDK_VERSION=13
ARG MVN_VERSION=3.6.0

FROM maven:${MVN_VERSION}-jdk-${JDK_VERSION} AS builder
ARG IMAGE_TAG
WORKDIR /usr/src/build
COPY .git ./.git
COPY pom.xml .
RUN mvn dependency:go-offline -B
COPY src ./src
#RUN yum install -y openssl && openssl req -passout pass:"changeit" -out app.csr -new -newkey rsa:2048 -keyout app.key -subj "/C=AU/CN=localhost" \
#    && openssl x509 -req -days 3650 -in app.csr -signkey app.key -passin pass:"changeit" -out app.crt \
#    && keytool -import -file app.crt -alias app -keystore app.jks -storepass changeit -noprompt \
#    && cp app.jks src/main/resources/
RUN keytool \
    -genkey \
    -alias app \
    -keyalg RSA \
    -keysize 2048 \
    -validity 3650 \
    -storepass changeit \
    -keystore app.jks \
    -noprompt \
    -dname "cn=John Galt, ou=Docker, o=Home, c=AU" \
    && cp app.jks src/main/resources/ \
    && mvn -f pom.xml clean package -Dversion=$IMAGE_TAG


FROM openjdk:${JDK_VERSION}-alpine
ARG IMAGE_TAG
EXPOSE 8443
ENV JAR_NAME="myproject-${IMAGE_TAG}.jar"
WORKDIR /usr/src/app
COPY --from=builder ./usr/src/build/target/${JAR_NAME} app.jar
ENTRYPOINT ["java"]
CMD ["-jar", "app.jar"]