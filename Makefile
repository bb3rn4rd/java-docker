BUILD_ID ?= 1
BUILD_SHA1 = $(shell git rev-parse --short=7 --verify HEAD)
BUILD_BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
MAJOR ?= 1
MINOR ?= 0
ifeq ($(BUILD_BRANCH),master)
	PATCH = $(BUILD_ID)
else
	PATCH = $(BUILD_ID)-$(BUILD_SHA1)
endif
IMAGE_TAG := $(MAJOR).$(MINOR).$(PATCH)

clean:
	IMAGE_TAG=$(IMAGE_TAG) \
    docker-compose --project-name cicd down || true
.PHONY: clean

build: clean
	IMAGE_TAG=$(IMAGE_TAG) \
	docker-compose build
.PHONY: build

test: build
	IMAGE_TAG=$(IMAGE_TAG) \
	docker-compose --project-name cicd up --detach
	E=$$(docker wait sut-$(IMAGE_TAG)) && exit $$E

	@echo successfully built docker image
.PHONY: test

push: test
	@docker login --username=${DOCKER_USERNAME} --password=${DOCKER_PASSWORD}
	IMAGE_TAG=$(IMAGE_TAG) \
	docker-compose --project-name cicd push builder
.PHONY: push